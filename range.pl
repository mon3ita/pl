:- include('math.pl').

% Finds all numbers between given two.
range(From, To, []) :- From >= To, !.
range(From, To, [From|Tail]) :- NewFrom is From + 1, range(NewFrom, To, Tail).

% Finds all numbers with distance d (> 0), between given two.
drange(From, To, _, []) :- From > To, !.
drange(From, To, Dist, [From | Tail]) :- Dist > 0, NewFrom is From + Dist, drange(NewFrom, To, Dist, Tail).

% Finds all elements between given two in descending order.
downto(From, To, []) :- From < To, !.
downto(From, To, [From | Tail]) :- NewFrom is From - 1, downto(NewFrom, To, Tail).

% Finds all elements in a given distance (> 0), between given two in descending order.
ddownto(From, To, _, []) :- From < To, !.
ddownto(From, To, Dist, [From | Tail]) :- Dist > 0, NewFrom is From - Dist, ddownto(NewFrom, To, Dist, Tail).

% Finds all primes between given two numbers.
prange(From, To, []) :- From > To, !.
prange(From, To, [From|Tail]) :- prime(From), NewFrom is From + 1, prange(NewFrom, To, Tail), !.
prange(From, To, List) :- NewFrom is From + 1, prange(NewFrom, To, List).