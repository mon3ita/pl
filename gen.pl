:- include('math.pl').

% Inf generator.
gen(0).
gen(X):-gen(Y), X is Y + 1.

% Even number generator.
even(0).
even(X):-even(Y), X is Y + 2.

% Odd number generator.
odd(1).
odd(X):-odd(Y), X is Y + 2.

% Finds product in a given range.
product(From, To, Current, Current) :- From >= To, !.
product(From, To, Current, Result) :- NewFrom is From + 1, NewResult is Current * From, product(NewFrom, To, NewResult, Result).

% Finds factorial.
fact(N, 1) :- N =< 1, !.
fact(N, Result) :- NewN is N + 1, product(1, NewN, 1, Result).

% Fact generator.
fgen(X) :- gen(Y), fact(Y, X).

% Prime number generator.
pgen(X) :- gen(X), prime(X).