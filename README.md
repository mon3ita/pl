# Prolog

Implementations of some functions in Prolog.

## Implementations

* **Gen**
  * `odd/1`
  * `fgen/1`
  * `pgen/1`
* **List**
  * `size/2`
  * `product/2`
* **Math**
  * `divisors/2`
  * `prime/1`
* **Range**
  * `range/3`
  * `drange/4`
  * `downto/3`
  * `ddownto/4`
  * `prange/3`

