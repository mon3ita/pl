
% Finds all divisors of a number, and puts them in a list.
divisors_(_, [], []).
divisors_(Number, [Head|Tail], [Head|Other]) :- 0 is mod(Number, Head), divisors_(Number, Tail, Other), !.
divisors_(Number, [_|Tail], Other) :- divisors_(Number, Tail, Other).

% Help function
range_(From, To, []) :- From >= To, !.
range_(From, To, [From|Tail]) :- NewFrom is From + 1, range_(NewFrom, To, Tail).

% Finds all divisors of a number (< Number), and puts them in a list.
divisors(Number, Result) :- Mid is (Number / 2) + 1, range_(2, Mid, Range), divisors_(Number, Range, Result).

% Checks if a given number is prime.
prime(Number) :- Number > 1, divisors(Number, Result), length(Result, Len), Len == 0.
