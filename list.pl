
% Size of a list.
size(List, Size) :- length(List, Size).

% Help function.
product_([], Current, Current).
product_([Head | Tail], Current, Result) :- NewCurrent is Current * Head, product_(Tail, NewCurrent, Result).

% Finds the product of all elements in a list.
product(List, Result) :- product_(List, 1, Result).