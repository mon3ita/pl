
% Adds an element at the end of a list.
append([], E, [E]).
append([H|T], E, [H|NT]) :- append(T, E, NT).

% Inserts an element at position N.
insertat(N, E, T, [E|T]) :- N is 0.
insertat(N, E, T, [0|NT]) :- length(T, SIZET),
                                SIZET is 0, N > 0,
                                NEWN is N-1, insertat(NEWN, E, T, NT).
insertat(N, E, [H|T], [H|NT]) :- NEWN is N-1, insertat(NEWN, E, T, NT).

% Inserts an element before an element.
insertbefore(_, _, [], []).
insertbefore(EL, E, [EL|T], [E, EL|T]).
insertbefore(EL, E, [H|T], [H|NT]) :- insertbefore(EL, E, T, NT).

% Inserts an element before Nth occurence of an element.
insertbeforenth(_, _, _, [], []).
insertbeforenth(N, EL, E, [EL|T], [E, EL|T]) :- N is 1.
insertbeforenth(N, EL, E, [EL|T], [EL|NT]) :- NEWN is N-1, 
                                                insertbeforenth(NEWN, EL, E, T, NT).
insertbeforenth(N, EL, E, [H|T], [H|NT]) :- insertbeforenth(N, EL, E, T, NT).

% Inserts an element after another.
insertafter(_, _, [], []).
insertafter(EL, E, [EL|T], [EL, E|T]).
insertafter(EL, E, [H|T], [H|NT]) :- insertafter(EL, E, T, NT).

% Inserts an element after the nth occurce of another.
insertafternth(_, _, _, [], []).
insertafternth(N, EL, E, [EL|T], [EL, E|T]) :- N is 1.
insertafternth(N, EL, E, [EL|T], [EL|NT]) :- NEWN is N-1, 
                                            insertafternth(NEWN, EL, E, T, NT).
insertafternth(N, EL, E, [H|T], [H|NT]) :- insertafternth(N, EL, E, T, NT).